PiSP - Pi Station Portable. 

Project which aims to create handheld console based on Rapsberry Pi [CM4](https://www.raspberrypi.org/products/compute-module-4/?variant=raspberry-pi-cm4001000) in the form factor of PSP ([PlayStation Portable](https://en.wikipedia.org/wiki/PlayStation_Portable)). 

Useful links:
- [Flashing onboard eMMC storage](https://www.raspberrypi.org/documentation/computers/compute-module.html#flashing-the-compute-module-emmc) + [simple video guide from one and only Jeff Geerling](https://www.youtube.com/watch?v=jp_mF1RknU4)
- [Enabling USB support (again a quick tutorial from Jeff Geerling)](https://www.jeffgeerling.com/blog/2020/usb-20-ports-not-working-on-compute-module-4-check-your-overlays)
- [Enabling DISP1 screen](https://www.raspberrypi.org/documentation/computers/compute-module.html#attaching-the-official-7-inch-display)
- [Installing Retropie](https://retropie.org.uk/docs/Manual-Installation/)
- [Newest vesrion of PPSSPP not working - fix](https://www.raspberrypi.org/documentation/computers/compute-module.html#attaching-the-official-7-inch-display)
- [How to get ROMs for RetroPie?](https://www.imagediamond.com/blog/best-safe-rom-downloading-websites-in-2020/)
