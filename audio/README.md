Useful links:
- [Explanation of RasPi audio frontend by Hackaday](https://hackaday.com/2018/07/13/behind-the-pin-how-the-raspberry-pi-gets-its-audio/)
- [Audio jack basics for Rapsberry Pi](https://www.raspberrypi-spy.co.uk/2014/07/raspberry-pi-model-b-3-5mm-audiovideo-jack/)
- [Pi 4b schematics (audio frontend)](https://datasheets.raspberrypi.org/rpi4/raspberry-pi-4-reduced-schematics.pdf)
- [Buffer used in the Pi models](https://www.onsemi.com/pdf/datasheet/nc7wz16-d.pdf)

PCB render:

![](images/pisp_audio_proto.png)
