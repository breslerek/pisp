Project of standalone gamepad which is set to be integrated later in the whole console. Gamepad is based on Raspberry Pi Pico, which controls all buttons and analog sticks. CircuitPython HID library is used to make it appear as a gamepad to standard PC. 

Useful links:
- [CricuitPython v6 tutorial from Adafruit](https://circuitpython.readthedocs.io/projects/hid/en/latest/)
- [HID library reference](https://learn.adafruit.com/getting-started-with-raspberry-pi-pico-circuitpython/overview)
- [Getting started with MicroPython on Rapsberry Pi Pico](https://datasheets.raspberrypi.org/pico/raspberry-pi-pico-python-sdk.pdf)
- [Raspberry Pi Pico documentation](https://datasheets.raspberrypi.org/pico/pico-datasheet.pdf)
- [RP2040 datasheet](https://datasheets.raspberrypi.org/rp2040/rp2040-datasheet.pdf)
- [Hardware design with RP2040](https://datasheets.raspberrypi.org/rp2040/hardware-design-with-rp2040.pdf)
- [Pico for KiCAD library](https://github.com/ncarandini/KiCad-RP-Pico)
Raspberry Pi Pico pinout:

![](images/Pico-Pinout.png)

First prototype:

![](images/prototype.JPG)

PCB render:

![](images/pcb_kicad.png)
